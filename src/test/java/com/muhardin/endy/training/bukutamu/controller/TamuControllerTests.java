package com.muhardin.endy.training.bukutamu.controller;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.muhardin.endy.training.bukutamu.controller.pageobject.FormInputTamu;
import com.muhardin.endy.training.bukutamu.dao.TamuDao;
import com.muhardin.endy.training.bukutamu.entity.Kota;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import io.github.bonigarcia.wdm.WebDriverManager;


@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@Sql(
    statements = "delete from tamu where email = 'tamu001@yopmail.com'", 
    scripts = {"classpath:/sql/clear-data-tamu.sql", "classpath:/sql/sample-data-tamu.sql"}
)
public class TamuControllerTests {

    private static final String URL_DATA_TAMU = "/api/tamu/";
    private static final String URL_FORM_TAMU = "/tamu/form";
    private static final List<String> SELENIUM_LOG_PACKAGES 
        = Arrays.asList("org.apache", "io.github.bonigarcia", "io.netty", "org.asynchttpclient");
    @LocalServerPort
	private int port;

    @Autowired private TamuDao tamuDao;
    @Autowired private ObjectMapper objectMapper;

    private static WebDriver webDriver;

    @BeforeAll
    public static void setupChromeDriverSelenium(){
        for(String logger : SELENIUM_LOG_PACKAGES) {
            ((Logger)LoggerFactory.getLogger(logger)).setLevel(Level.INFO);
        }
        
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        options.setHeadless(true);
        webDriver = new ChromeDriver(options);
    }

    @AfterAll
    public static void tutupBrowser(){
        webDriver.quit();
    }

    @Test
    public void testApiDataTamu(@Autowired WebTestClient webClient) throws Exception {
        String jsonDariDb = objectMapper.writeValueAsString(tamuDao.findAll());
        webClient
            .get().uri(URL_DATA_TAMU).exchange()
            .expectStatus().isOk()
            .expectBody(String.class)
            .isEqualTo(jsonDariDb);
    }

    @Test
    public void testFormInputTamu() throws Exception{
        webDriver.get("http://localhost:"+port+URL_FORM_TAMU);
        
        FormInputTamu formInputTamu = new FormInputTamu(webDriver);
        Kota kota = new Kota();
        kota.setId(3L);
        
        String nama = "Tamu 991";
        String email = "tamu991@yopmail.com";

        formInputTamu.inputTamu(nama, email, kota);
        List<WebElement> namaTamu = webDriver.findElements(By.xpath("//*[contains(text(),'"+nama+"')]"));
        System.out.println("Jumlah element yang ditemukan : "+namaTamu.size());

        TakesScreenshot scrShot =((TakesScreenshot)webDriver);
        File screenshot = scrShot.getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(screenshot, new File("target/screenshot.png"));

        Assertions.assertFalse(namaTamu.isEmpty());
    }
}
