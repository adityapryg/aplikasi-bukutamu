package com.muhardin.endy.training.bukutamu.dao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.muhardin.endy.training.bukutamu.entity.Kota;

@SpringBootTest
public class KotaDaoTests {

    @Autowired private KotaDao kotaDao;

    @Test
    public void testFindByNama(){
        Kota k = kotaDao.findByNama("Jakarta");
        Assertions.assertNotNull(k);
        Assertions.assertEquals("JKT", k.getKode());

        Assertions.assertNull(kotaDao.findByNama("Yogyakarta"));
    }
}
