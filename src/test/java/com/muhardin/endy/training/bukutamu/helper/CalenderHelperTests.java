package com.muhardin.endy.training.bukutamu.helper;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CalenderHelperTests {

    @Test
    public void testJumlahHari(){
        Assertions.assertEquals(31, CalendarHelper.jumlahHari(2022, 1));
        Assertions.assertEquals(30, CalendarHelper.jumlahHari(2022, 4));
    }
}
