package com.muhardin.endy.training.bukutamu.controller.pageobject;

import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.muhardin.endy.training.bukutamu.entity.Kota;

public class FormInputTamu {
    private WebDriver webDriver;

    @FindBy(name = "nama")
    private WebElement txtNama;

    @FindBy(name = "email")
    private WebElement txtEmail;

    @FindBy(name = "kota")
    private WebElement cmbKota;

    @FindBy(id = "btnSimpan")
    private WebElement btnSimpan;

    public FormInputTamu(WebDriver webDriver){
        this.webDriver = webDriver;
        PageFactory.initElements(webDriver, this);
    }

    public void inputTamu(String nama, String email, Kota kota) {
        txtNama.sendKeys(nama);
        txtEmail.sendKeys(email);
        Select slKota = new Select(cmbKota);
        slKota.selectByValue(String.valueOf(kota.getId()));
        btnSimpan.click();
        new WebDriverWait(webDriver, Duration.ofSeconds(5))
        .until(ExpectedConditions.titleIs("Daftar Tamu"));
    }
}
