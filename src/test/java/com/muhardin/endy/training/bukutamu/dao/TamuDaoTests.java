package com.muhardin.endy.training.bukutamu.dao;

import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

import com.muhardin.endy.training.bukutamu.entity.Kota;
import com.muhardin.endy.training.bukutamu.entity.Tamu;

@SpringBootTest
@Sql(
    statements = "delete from tamu where email = 'tamu001@yopmail.com'", 
    scripts = {"classpath:/sql/clear-data-tamu.sql", "classpath:/sql/sample-data-tamu.sql"}
)
public class TamuDaoTests {
    @Autowired private TamuDao tamuDao;
    @Autowired private KotaDao kotaDao;

    @Test
    public void testSimpanDataTamu(){
        Tamu t = new Tamu();
        t.setNama("Tamu 001");
        t.setEmail("tamu001@yopmail.com");
        
        Optional<Kota> k = kotaDao.findById(1L);
        Assertions.assertTrue(k.isPresent());
        t.setKota(k.get());

        tamuDao.save(t);
        Assertions.assertNotNull(t.getId());
        System.out.println("ID tamu hasil generate : "+t.getId());
    }

    @Test
    public void testHitungTamu(){
        Long jumlahTamu = tamuDao.count();
        Assertions.assertEquals(2, jumlahTamu);
    }
}
