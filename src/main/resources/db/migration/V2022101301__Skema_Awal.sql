create table kota (
    id bigserial,
    kode varchar(50) not null,
    nama varchar(100) not null,
    primary key (id),
    unique (kode)
);

create table tamu (
    id varchar(36),
    nama varchar(255) not null,
    email varchar(100) not null,
    id_kota bigint not null,
    primary key (id),
    unique (email),
    foreign key (id_kota) references kota
);