FROM --platform=linux/x86_64 openjdk:17-alpine
ADD target/aplikasi-bukutamu-0.0.1-SNAPSHOT.jar /opt/app.jar
RUN sh -c 'touch /opt/app.jar'
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/opt/app.jar"]