# Aplikasi Buku Tamu #

Fitur :

* Input Data Tamu

    * Nama
    * Email
    * Upload Foto

* List Data Tamu

## Cara Menjalankan Database Development ##

1. Buka terminal / command prompt

2. Masuk ke folder project

    ```
    cd workspace/training/aplikasi-bukutamu
    ```

3. Jalankan docker compose

    ```
    docker-compose up
    ```

4. Connect ke database development

    ```
    psql -h localhost -U bukutamu bukutamudb
    Password for user bukutamu:
    bukutamu123
    ```

## Cara Membuat Docker Image ##

1. Build image

    ```
    docker build -t endymuhardin/aplikasi-bukutamu .
    ```

    Khusus Docker Desktop for Mac, ada error berikut

    ```
    failed to solve with frontend dockerfile.v0: failed to create LLB definition: no match for platform in manifest
    ```

    Solusinya adalah dengan memasang environment variable berikut

    ```
    export DOCKER_BUILDKIT=0
    export COMPOSE_DOCKER_CLI_BUILD=0
    ```

    Bila muncul error berikut di Macbook M1 / Apple Silicon

    ```
    no matching manifest for linux/arm64/v8 in the manifest list entries
    ```

    Solusinya adalah menambahkan opsi di baris `FROM` pada `Dockerfile` menjadi seperti ini

    ```
    FROM --platform=linux/x86_64 openjdk:17-alpine
    ```

2. Lihat daftar images yang ada di komputer kita

    ```
    docker images
    ```

    Pastikan images yang dibuild sudah terdaftar

    ```
    REPOSITORY                       TAG           IMAGE ID       CREATED          SIZE
    endymuhardin/aplikasi-bukutamu   latest        f101c94e0c9b   35 seconds ago   420MB
    ```

3. Jalankan image yang baru saja dibuat

    ```
    docker run --rm -it --p 8080:8080 --platform=linux/x86_64 -e SPRING_DATASOURCE_URL=jdbc:postgresql://10.250.37.1/bukutamudb endymuhardin/aplikasi-bukutamu
    ```

    Jangan lupa ganti IP `10.250.37.1` dengan IP address laptop/PC